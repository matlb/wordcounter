# Word Counter
## How to Build
`mvn clean install`

## How to run
`java -jar /path/to/jar $filename`
Where $filename is the name of the file containing the words to be counted.
For example:
`java -jar target/wordcounter-1.0-SNAPSHOT.jar testWords`

Note: A built jar and test file have been provided
