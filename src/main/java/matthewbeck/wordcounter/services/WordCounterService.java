package matthewbeck.wordcounter.services;

import matthewbeck.wordcounter.models.WordCount;

import java.util.List;

public interface WordCounterService {
    List<WordCount> countWords(List<String> words);

    List<WordCount> sortWordCounts(List<WordCount> words);
}
