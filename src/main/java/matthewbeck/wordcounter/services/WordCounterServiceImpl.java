package matthewbeck.wordcounter.services;

import matthewbeck.wordcounter.models.WordCount;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class WordCounterServiceImpl implements WordCounterService {
    @Override
    public List<WordCount> countWords(List<String> words) {
        return new HashSet<>(words).stream()
                .map(word -> new WordCount(word, Collections.frequency(words, word)))
                .collect(Collectors.toList());
    }

    @Override
    public List<WordCount> sortWordCounts(List<WordCount> words) {
        return words.stream()
                .sorted()
                .collect(Collectors.toList());
    }
}
