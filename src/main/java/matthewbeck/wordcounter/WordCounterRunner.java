package matthewbeck.wordcounter;

import matthewbeck.wordcounter.displays.Displayer;
import matthewbeck.wordcounter.files.FileParser;
import matthewbeck.wordcounter.services.WordCounterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class WordCounterRunner implements CommandLineRunner {
    private static final Logger LOG = LoggerFactory.getLogger(WordCounterRunner.class);

    private final FileParser fileParser;

    private final WordCounterService wordCounterService;

    private final Displayer displayer;

    @Autowired
    public WordCounterRunner(FileParser fileParser, WordCounterService wordCounterService, Displayer displayer) {
        this.fileParser = fileParser;
        this.wordCounterService = wordCounterService;
        this.displayer = displayer;
    }

    @Override
    public void run(String... args) {
        if (validateArgs(args)) {
            LOG.info("Starting Word Counter");
            var words = fileParser.getWordsFromFile(args[0]);
            LOG.debug(String.format("Parsed file: %s", words));

            var wordCounts = wordCounterService.countWords(words);
            LOG.debug(String.format("Unsorted word counts: %s", wordCounts));

            var sortedWorldCounts = wordCounterService.sortWordCounts(wordCounts);
            LOG.debug(String.format("Sorted word counts: %s", sortedWorldCounts));

            displayer.displayWordCounts(sortedWorldCounts);
            LOG.info("Finished Word Counter");
        } else {
            System.out.println("You must provide a single argument of a filename");
        }
    }

    private boolean validateArgs(String... args) {
        return args.length == 1;
    }
}
