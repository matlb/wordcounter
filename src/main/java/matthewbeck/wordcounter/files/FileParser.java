package matthewbeck.wordcounter.files;

import java.util.List;

public interface FileParser {
    List<String> getWordsFromFile(String filename);
}
