package matthewbeck.wordcounter.files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class NIOFileParser implements FileParser {
    private static final Logger LOG = LoggerFactory.getLogger(NIOFileParser.class);

    private static final String WHITESPACE_REGEX = "[\\n\\r\\s]+";

    public List<String> getWordsFromFile(String filename) {
        return readStringFromFile(filename)
                .map(this::splitIntoWords)
                .orElseGet(Collections::emptyList);
    }

    private Optional<String> readStringFromFile(String filename) {
        try {
            return Optional.of(Files.readString(Path.of(filename)));
        } catch (IOException e) {
            LOG.error(String.format("Error reading file: %s", e));
            return Optional.empty();
        }
    }

    private List<String> splitIntoWords(String string) {
        return List.of(string.split(WHITESPACE_REGEX));
    }
}
