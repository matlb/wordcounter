package matthewbeck.wordcounter.displays;

import matthewbeck.wordcounter.models.WordCount;

import java.util.List;

public interface Displayer {
    void displayWordCounts(List<WordCount> wordCounts);
}
