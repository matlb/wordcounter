package matthewbeck.wordcounter.displays;

import matthewbeck.wordcounter.models.WordCount;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StdOutDisplayer implements Displayer {

    @Override
    public void displayWordCounts(List<WordCount> wordCounts) {
        wordCounts.forEach(System.out::println);
    }
}
