package matthewbeck.wordcounter.models;

import java.util.Objects;

public class WordCount implements Comparable<WordCount> {
    private final String word;

    private final int count;

    public WordCount(String word, int count) {
        this.word = word;
        this.count = count;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", word, count);
    }

    @Override
    public int compareTo(WordCount other) {
        return Integer.compare(other.count, this.count);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WordCount wordCount = (WordCount) o;
        return count == wordCount.count &&
                Objects.equals(word, wordCount.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word, count);
    }
}
