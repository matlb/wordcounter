package matthewbeck.wordcounter.files;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class NIOFileParserTest {

    private final FileParser fileParser = new NIOFileParser();

    @Test
    public void shouldGetWordsFromFileOfWords() throws IOException {
        var actualWords = fileParser.getWordsFromFile(new ClassPathResource("words").getFile().getPath());
        var expectedWords = List.of("this", "is", "a", "test", "of", "some", "test", "words", "that", "is", "a", "test");

        assertEquals(expectedWords, actualWords);
    }

    @Test
    public void shouldReturnEmptyForNonExistingFile() {
        var actualWords = fileParser.getWordsFromFile("Non Exiting File");

        assertThat(actualWords, empty());
    }
}
