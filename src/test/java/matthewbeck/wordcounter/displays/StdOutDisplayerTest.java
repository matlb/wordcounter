package matthewbeck.wordcounter.displays;

import matthewbeck.wordcounter.models.WordCount;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StdOutDisplayerTest {

    @Test
    public void shouldOutputsWordCountsToStdOut() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        var displayer = new StdOutDisplayer();
        var wordCounts = List.of(new WordCount("a", 3), new WordCount("this", 2), new WordCount("test", 1));

        displayer.displayWordCounts(wordCounts);

        var expectedOutput = "a: 3\n" +
                "this: 2\n" +
                "test: 1\n";
        assertEquals(expectedOutput, outContent.toString());
    }
}
