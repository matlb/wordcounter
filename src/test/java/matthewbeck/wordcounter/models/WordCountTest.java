package matthewbeck.wordcounter.models;

import org.junit.Test;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class WordCountTest {

    @Test
    public void toStringShouldFormatCorrectly() {
        var wordCount = new WordCount("Word", 13);
        var expectedString = "Word: 13";
        assertEquals(wordCount.toString(), expectedString);
    }

    @Test
    public void compareToShouldIdentifiesHigherWordCount() {
        var wordCountHigher = new WordCount("Word", 13);
        var wordCountLower = new WordCount("Other", 9);

        assertThat(wordCountLower.compareTo(wordCountHigher), greaterThan(0));
    }
}
