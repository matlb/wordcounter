package matthewbeck.wordcounter;

import matthewbeck.wordcounter.displays.Displayer;
import matthewbeck.wordcounter.files.FileParser;
import matthewbeck.wordcounter.models.WordCount;
import matthewbeck.wordcounter.services.WordCounterService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class WordCounterRunnerTest {

    @Mock
    private FileParser fileParser;

    @Mock
    private WordCounterService wordCounterService;

    @Mock
    private Displayer displayer;

    @Test
    public void runShouldParseFileCountAndOrderWordsAndDisplay() {
        var wordCounterRunner = new WordCounterRunner(fileParser, wordCounterService, displayer);
        var filename = "words";
        var testWord1 = "a";
        var testWord2 = "test";
        var testWords = List.of(testWord1, testWord2, testWord2);
        var testWordCount1 = new WordCount(testWord1, 1);
        var testWordCount2 = new WordCount(testWord2, 2);

        when(fileParser.getWordsFromFile(filename)).thenReturn(testWords);
        when(wordCounterService.countWords(testWords))
                .thenReturn(List.of(testWordCount1, testWordCount2));
        when(wordCounterService.sortWordCounts(List.of(testWordCount1, testWordCount2)))
                .thenReturn(List.of(testWordCount2, testWordCount1));

        wordCounterRunner.run(filename);

        verify(fileParser).getWordsFromFile(filename);
        verify(wordCounterService).countWords(testWords);
        verify(wordCounterService).sortWordCounts(List.of(testWordCount1, testWordCount2));
        verify(displayer).displayWordCounts(List.of(testWordCount2, testWordCount1));
    }

    @Test
    public void runShouldNotCountWordsIfNoArgs() {
        new WordCounterRunner(fileParser, wordCounterService, displayer).run();

        verifyZeroInteractions(fileParser);
        verifyZeroInteractions(wordCounterService);
        verifyZeroInteractions(displayer);
    }

    @Test
    public void runShouldNotCountWordsIfMoreThanOneArgs() {
        new WordCounterRunner(fileParser, wordCounterService, displayer).run("words", "words");

        verifyZeroInteractions(fileParser);
        verifyZeroInteractions(wordCounterService);
        verifyZeroInteractions(displayer);
    }
}
