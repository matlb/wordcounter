package matthewbeck.wordcounter.services;

import matthewbeck.wordcounter.models.WordCount;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertThat;

public class WordCounterServiceImplTest {

    private final WordCounterService wordCounterService = new WordCounterServiceImpl();

    @Test
    public void countWordsShouldCreateWordCountsFromGivenWords() {
        var words = List.of("this", "is", "a", "test", "of", "some", "test", "words", "that", "is", "a", "test");
        var wordCounts = wordCounterService.countWords(words);

        assertThat(wordCounts, containsInAnyOrder(new WordCount("a", 2), new WordCount("this", 1),
                new WordCount("test", 3), new WordCount("some", 1), new WordCount("words", 1),
                new WordCount("is", 2), new WordCount("that", 1), new WordCount("of", 1)));
    }

    @Test
    public void sortWordCountsShouldSortWordCountsDescending() {
        var wordCounts = List.of(new WordCount("a", 2), new WordCount("this", 1), new WordCount("test", 3));
        var sortedWordCounts = wordCounterService.sortWordCounts(wordCounts);

        assertThat(sortedWordCounts, contains(new WordCount("test", 3),new WordCount("a", 2), new WordCount("this", 1)));
    }
}
